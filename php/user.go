package php

import (
	"encoding/json"
)

const (
	userListUrl = "http://api.mypep.cn/inside/user/list"
)

type UserList struct {
	TotalPage  int32 `json:"string"`
	PageCount  int32 `json:"string"`
	TotalCount int64
	Order      string
	List       []User
}
type User struct {
	Avatar     string
	SchoolId   int32 `json:"school_id"`
	Xueduan    string
	Username   string
	Truename   string
	DistrictId int32  `json:"district_id"`
	SourceFrom string `json:"source_from"`
}

func GetUserList(params map[string]string) (users UserList, err error) {
	data, err := httpGet(userListUrl, params)
	if err != nil {
		return
	}
	err = json.Unmarshal(data, &users)
	return
}
