package php

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
)

type Resp struct {
	Status int
	Msg    string
	Result json.RawMessage
}

func httpGet(url string, params map[string]string) (res []byte, err error) {
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return
	}
	q := req.URL.Query()
	for k, v := range params {
		q.Add(k, v)
	}
	req.URL.RawQuery = q.Encode()
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return
	}
	defer resp.Body.Close()
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return
	}
	var r Resp
	err = json.Unmarshal(data, &r)
	if err != nil {
		return
	}
	if r.Status == 1 {
		return r.Result, nil
	}
	err = errors.New(r.Msg)
	return
}
